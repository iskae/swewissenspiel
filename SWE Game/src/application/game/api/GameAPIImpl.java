package application.game.api;

public class GameAPIImpl implements GameAPI {

    /**
     * @directed true
     * @label -game
     * @link composition
     * @supplierCardinality 1
     */
    private static GameAPIImpl gameAPI;
    private GameImpl game;

    public GameAPIImpl() {
        this.game = new GameImpl(this);
    }

    public static GameAPI makeGameAPI() {
        if (GameAPIImpl.gameAPI == null) {
            GameAPIImpl.gameAPI = new GameAPIImpl();
        }
        return GameAPIImpl.gameAPI;
    }

    @Override
    public final Game getGame() {
        if (this.game == null) {
            this.game = new GameImpl(this);
        }
        return this.game;
    }
    
    @Override
    public final Subject getGameSub() {
        if (this.game == null) {
            this.game = new GameImpl(this);
        }
        return this.game;
    }

    @Override
    public final void chooseStarter() {
        this.game.chooseStarter();
    }

    @Override
    public final void dice() {
        this.game.dice();
    }

    @Override
    public final void moveFigure(int index) {
        this.game.moveFigure(index);
    }

    @Override
    public final void showStats() {
        this.game.showStats();
    }
}
