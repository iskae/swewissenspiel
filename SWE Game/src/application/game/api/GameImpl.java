package application.game.api;

import application.game.api.events.Conflict;
import application.game.api.events.Dice;
import application.game.api.events.Counter;
import application.game.api.events.Figure;
import application.game.api.events.FigureRequest;
import application.gui.observers.Observer;
import application.game.api.events.Player;
import application.game.stateMachine.StateMachineDice;
import application.game.api.events.Stats;
import application.game.api.events.Info;
import application.game.api.events.Move;
import application.game.api.events.QuestionRound;
import java.util.ArrayList;

public class GameImpl implements Game, Subject {

    private final ArrayList<Observer<Info>> observers = new ArrayList<>();

    private StateMachineDice smDice = null;

    private Player currentPlayer;
    private Dice currentDice;
    private Counter currentCounter;

    private Field[] gameField;
    private Field[][] homeFields;
    private Field[][][] knowledgeFields;

    private final GameAPIImpl gameAPIImpl;

    GameImpl(GameAPIImpl gameAPIImpl) {
        this.gameAPIImpl = gameAPIImpl;
        initGame();
    }

    private void initGame() {
        gameField = new Field[48];
        homeFields = new Field[4][3];
        knowledgeFields = new Field[4][4][3];
        for (int fields = 0; fields < 48; fields++) {
            gameField[fields] = new Field(null);
        }
        for (int player = 0; player < 4; player++) {
            for (int index = 0; index < 3; index++) {
                homeFields[player][index] = new Field(new Figure(index, new Player(player)));
            }
            for (int category = 0; category < 4; category++) {
                knowledgeFields[player][category][0] = new Field(new Figure(category, new Player(player)));
            }
        }
        if (smDice == null) {
            smDice = new StateMachineDice();
        }
    }

    @Override
    public void attach(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notify(Object arg) {
        if (arg instanceof Info) {
            observers.stream().forEach((observer) -> {
                observer.update((Info) arg);
            });
        }
    }

    @Override
    public void chooseStarter() {
        if (currentPlayer == null) {
            currentPlayer = new Player(0);
            notify(currentPlayer);
            currentCounter = new Counter(3);
            notify(currentCounter);
        }
    }

    @Override
    public void dice() {
        if (smDice.getcurrentStateName().equals("WaitForDice")
                || smDice.getcurrentStateName().equals("WaitForReroll")) {
            doDice();
        }
        if (smDice.getcurrentStateName().equals("ProcessingDice")) {
            if (currentDice.getDice() != 6) {
                if (homeFields[currentPlayer.getPlayer()][0].getOccupier() == null || homeFields[currentPlayer.getPlayer()][1].getOccupier() == null || homeFields[currentPlayer.getPlayer()][2].getOccupier() == null) {
                    sendFigureRequest();
                } else {
                    currentCounter = new Counter(currentCounter.getCount() - 1);
                    if (currentCounter.getCount() != 0) {
                        changeCounter(false);
                    } else {
                        nextPlayer();
                        changeCounter(true);
                    }
                    smDice.transverse("WaitForReroll");
                }
            } else {
                if (homeFields[currentPlayer.getPlayer()][0].getOccupier() == null && homeFields[currentPlayer.getPlayer()][1].getOccupier() == null && homeFields[currentPlayer.getPlayer()][2].getOccupier() == null) {
                    sendFigureRequest();
                    changeCounter(true);
                } else {
                    int startfield = fetchStarField(currentPlayer);
                    if (homeFields[currentPlayer.getPlayer()][0].getOccupier() != null) {
                        moveFromHome(homeFields[currentPlayer.getPlayer()][0], gameField[startfield]);
                    } else {
                        if (homeFields[currentPlayer.getPlayer()][1].getOccupier() != null) {
                            moveFromHome(homeFields[currentPlayer.getPlayer()][1], gameField[startfield]);
                        } else {
                            if (homeFields[currentPlayer.getPlayer()][2].getOccupier() != null) {
                                moveFromHome(homeFields[currentPlayer.getPlayer()][2], gameField[startfield]);
                            }
                        }

                    }
                    nextPlayer();
                    changeCounter(true);
                }
            }
        }
    }

    @Override
    public void moveFigure(int index) {
        if (smDice.getcurrentStateName().equals("WaitForFigure")) {
            smDice.transverse("ProcessingFigure");
            boolean valid = false;
            for (int indexGameField = 0; indexGameField < 48; indexGameField++) {
                if (gameField[indexGameField].getOccupier() != null) {
                    if (gameField[indexGameField].getOccupier().getIndex() == index && gameField[indexGameField].getOccupier().getOwner().getPlayer() == currentPlayer.getPlayer()) {
                        if (gameField[((indexGameField + currentDice.getDice()) % 48)].getOccupier() == null || gameField[((indexGameField + currentDice.getDice()) % 48)].getOccupier().getOwner().getPlayer() != currentPlayer.getPlayer()) {
                            moveOnFied(gameField[indexGameField], gameField[((indexGameField + currentDice.getDice()) % 48)]);
                            valid = true;
                            nextPlayer();
                            changeCounter(true);
                            break;
                        }
                    }
                }
            }
            if (!valid) {
                sendFigureRequest();
            }
        }
    }

    @Override
    public void showStats() {
        for (int players = 0; players < 4; players++) {
            Player tempPlayer = new Player(players);
            ArrayList<Figure> listfigures = new ArrayList<>();
            ArrayList<Integer> listindices = new ArrayList<>();
            fetchAllFiguresOfPlayer(tempPlayer, listfigures);
            fetchAllFieldsOfPlayer(tempPlayer, listindices);
            Stats tempStats = new Stats(tempPlayer, listfigures, listindices);
            notify(tempStats);
        }
    }

    private void nextPlayer() {
        if (currentPlayer.getPlayer() + 1 < 4) {
            currentPlayer = new Player(currentPlayer.getPlayer() + 1);
        } else {
            currentPlayer = new Player(0);
        }
        notify(currentPlayer);
        showStats();
        smDice.transverse("WaitForDice");
    }

    private void changeCounter(boolean reset) {
        if (reset) {
            currentCounter = new Counter(3);
        }
        notify(currentCounter);
    }

    private void moveFromHome(Field from, Field to) {
        Conflict con;
        if (to.getOccupier() == null) {
            con = new Conflict(from.getOccupier().getOwner(), null);
        } else {
            con = new Conflict(from.getOccupier().getOwner(), to.getOccupier().getOwner());
        }
        if (con.getVictim() != null) {
            if (con.getVictim().getPlayer() == currentPlayer.getPlayer()) {
                notify(con);
            } else {
                notify(con);
                homeFields[con.getVictim().getPlayer()][to.getOccupier().getIndex()].setOccupier(to.getOccupier());
                int startfield = fetchStarField(con.getVictim());
                if (gameField[startfield].getOccupier() == null) {
                    if (homeFields[con.getVictim().getPlayer()][0].getOccupier() != null) {
                        moveFromHome(homeFields[con.getVictim().getPlayer()][0], gameField[startfield]);
                    } else {
                        if (homeFields[con.getVictim().getPlayer()][1].getOccupier() != null) {
                            moveFromHome(homeFields[con.getVictim().getPlayer()][1], gameField[startfield]);
                        } else {
                            if (homeFields[con.getVictim().getPlayer()][2].getOccupier() != null) {
                                moveFromHome(homeFields[con.getVictim().getPlayer()][2], gameField[startfield]);
                            }
                        }
                    }
                }
                notify(new QuestionRound());
                makeMove(from, to);
                from.setOccupier(null);
            }
        } else {
            makeMove(from, to);
            from.setOccupier(null);
        }
    }

    private void moveOnFied(Field from, Field to) {
        Conflict con;
        if (to.getOccupier() == null) {
            con = new Conflict(from.getOccupier().getOwner(), null);
        } else {
            con = new Conflict(from.getOccupier().getOwner(), to.getOccupier().getOwner());
        }
        if (con.getVictim() != null) {
            if (con.getVictim().getPlayer() == currentPlayer.getPlayer()) {
                notify(con);
                sendFigureRequest();
            } else {
                notify(con);
                homeFields[con.getVictim().getPlayer()][to.getOccupier().getIndex()].setOccupier(to.getOccupier());
                int startfield = fetchStarField(con.getVictim());
                if (gameField[startfield].getOccupier() == null) {
                    if (homeFields[con.getVictim().getPlayer()][0].getOccupier() != null) {
                        moveFromHome(homeFields[con.getVictim().getPlayer()][0], gameField[startfield]);
                    } else {
                        if (homeFields[con.getVictim().getPlayer()][1].getOccupier() != null) {
                            moveFromHome(homeFields[con.getVictim().getPlayer()][1], gameField[startfield]);
                        } else {
                            if (homeFields[con.getVictim().getPlayer()][2].getOccupier() != null) {
                                moveFromHome(homeFields[con.getVictim().getPlayer()][2], gameField[startfield]);
                            }
                        }
                    }
                }
                notify(new QuestionRound());
                makeMove(from, to);
                from.setOccupier(null);
            }
        } else {
            makeMove(from, to);
            from.setOccupier(null);
        }
    }

    private void makeMove(Field from, Field to) {
        notify(new Move(from, to));
    }

    private void sendFigureRequest() {
        notify(new FigureRequest());
        smDice.transverse("WaitForFigure");
    }

    private void doDice() {
        currentDice = new Dice(genDice(), currentPlayer.getPlayer());
        notify(currentDice);
        smDice.transverse("ProcessingDice");
    }

    private int fetchStarField(Player player) {
        return (player.getPlayer() * 12);
    }

    private int genDice() {
        //totally random :)
        //return 6;
        return (int) ((Math.random() * 1000) % 6) + 1;
    }

    private void fetchAllFiguresOfPlayer(Player player, ArrayList<Figure> list) {
        for (Field gameField1 : gameField) {
            if (gameField1.getOccupier() != null) {
                if (gameField1.getOccupier().getOwner().getPlayer() == player.getPlayer()) {
                    list.add(gameField1.getOccupier());
                }
            }
        }
    }

    private void fetchAllFieldsOfPlayer(Player player, ArrayList<Integer> list) {
        ArrayList<Integer> temp = new ArrayList<>();
        for (int index = 0; index < gameField.length; index++) {
            if (gameField[index].getOccupier() != null) {
                if (gameField[index].getOccupier().getOwner().getPlayer() == player.getPlayer()) {
                    list.add(index);
                }
            }
        }
    }
}
