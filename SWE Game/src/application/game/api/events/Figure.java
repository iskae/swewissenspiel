package application.game.api.events;

public class Figure {
    private final int index;
    private final Player owner;

    public Figure(final int index, Player owner) {
        this.index = index;
        this.owner = owner;
    }

    public final int getIndex() {
        return this.index;
    }

    public final Player getOwner() {
        return this.owner;
    }

    @Override
    public final String toString() {
        switch (this.owner.getPlayer()) {
            case 0:
                return "R" + (index + 1);
            case 1:
                return "Y" + (index + 1);
            case 2:
                return "G" + (index + 1);
            case 3:
                return "B" + (index + 1);
            default:
                return "X" + (index + 1);
        }
    }
}
