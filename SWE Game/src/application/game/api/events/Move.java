package application.game.api.events;

import application.game.api.Field;

public class Move implements Info {

    private final Field from;
    private final Field to;

    public Move(final Field from, final Field to) {
        this.from = from;
        this.to = to;
    }

    public final Field getFrom() {
        return this.from;
    }

    public final Field getTo() {
        return this.to;
    }
}
