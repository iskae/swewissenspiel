package application.game.api.events;

public class Conflict implements Info {

    private final Player aggressor;
    private final Player victim;

    public Conflict(Player aggressor, Player victim) {
        this.aggressor = aggressor;
        this.victim = victim;
    }

    public final Player getAggressor() {
        return this.aggressor;
    }

    public final Player getVictim() {
        return this.victim;
    }
}
