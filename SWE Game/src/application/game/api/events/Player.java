package application.game.api.events;

public class Player implements Info {

    private int player;

    public Player(final int Player) {
        this.player = Player;
    }

    public final int getPlayer() {
        return this.player;
    }

    public final void setPlayer(final int Player) {
        this.player = Player;
    }

    @Override
    public final String toString() {
        switch (this.player) {
            case 0:
                return "Red";
            case 1:
                return "Yellow";
            case 2:
                return "Green";
            case 3:
                return "Blue";
            default:
                return "Unknown";
        }
    }
}
