package application.game.api;

import application.game.api.events.Figure;

public class Field {
    private Figure occupier;

    public Field() {
        this.occupier = null;
    }

    public Field(final Figure occupier) {
        this.occupier = occupier;
    }

    public void setOccupier(final Figure occupier) {
        this.occupier = occupier;
    }

    public Figure getOccupier() {
        return this.occupier;
    }
}
