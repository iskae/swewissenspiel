package application.game.api;

import application.gui.observers.Observer;

public interface Subject<Info> {

    void attach(Observer<Info> observer);

    void detach(Observer<Info> observer);

    void notify(Info arg);
}
