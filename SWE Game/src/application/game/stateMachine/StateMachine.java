package application.game.stateMachine;

import java.util.ArrayList;

public abstract class StateMachine {

    private ArrayList<State> states;
    private State currentState = new State("");

    public StateMachine() {
    }

    public final State getcurrentState() {
        return this.currentState;
    }

    public final ArrayList<State> getStates() {
        return this.states;
    }

    public final void setStates(final ArrayList<State> newStates) {
        this.states = newStates;
    }


    public final String getcurrentStateName() {
        return this.getcurrentState().getStateName();
    }

    public final void swtichState(final String name) {
        for (State s : states) {
            if (s.getStateName().equals(name)) {
                currentState = s;
                break;
            }
        }
    }

    public abstract void transverse(String name);
}
