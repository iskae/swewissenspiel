package application.game.stateMachine;

import java.util.ArrayList;

public class StateMachineDice extends StateMachine {

    public StateMachineDice() {
        this.setStates(new ArrayList<>());
        this.getStates().add(new State("WaitForDice"));
        this.getStates().add(new State("WaitForReroll"));
        this.getStates().add(new State("ProcessingDice"));
        this.getStates().add(new State("WaitForFigure"));
        this.getStates().add(new State("ProcessingFigure"));
        this.getStates().add(new State("Error"));
        this.swtichState("WaitForDice");
    }

    @Override
    public final void transverse(final String name) {
        switch (name) {
            case "WaitForDice":
                this.swtichState("WaitForDice");
                break;
            case "WaitForReroll":
                this.swtichState("WaitForReroll");
                break;
            case "ProcessingDice":
                this.swtichState("ProcessingDice");
                break;
            case "WaitForFigure":
                this.swtichState("WaitForFigure");
                break;
            case "ProcessingFigure":
                this.swtichState("ProcessingFigure");
                break;
            default:
                this.swtichState("Error");
                break;
        }
    }

}
