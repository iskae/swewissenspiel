package application.game.stateMachine;

import static application.game.api.GameAPIImpl.makeGameAPI;
import java.util.ArrayList;

public class StateMachineGUI extends StateMachine {

    public StateMachineGUI() {
        this.setStates(new ArrayList<>());
        this.getStates().add(new State("GameStarted"));
        this.getStates().add(new State("WaitForDice"));
        this.getStates().add(new State("DiceProcess"));
        this.getStates().add(new State("WaitForFigure"));
        this.getStates().add(new State("FigureProcess"));
        this.getStates().add(new State("CategoryRequest"));
        this.getStates().add(new State("NewCategoryRequest"));
        this.getStates().add(new State("Exit"));
        this.getStates().add(new State("Error"));
        this.swtichState("GameStarted");
    }

    @Override
    public final void transverse(final String name) {
        switch (name) {
            case "GameStarted":
                makeGameAPI();
                this.swtichState("GameStarted");
                break;
            case "WaitForDice":
                this.swtichState("WaitForDice");
                break;
            case "DiceProcess":
                this.swtichState("DiceProcess");
                break;
            case "WaitForFigure":
                this.swtichState("WaitForFigure");
                break;
            case "FigureProcess":
                this.swtichState("FigureProcess");
                break;
            case "CategoryRequest":
                this.swtichState("CategoryRequest");
                break;
            case "NewCategoryRequest":
                this.swtichState("NewCategoryRequest");
                break;
            case "Exit":
                this.swtichState("Exit");
                break;
            default:
                System.out.println("Something bad happend!");
                this.swtichState("Error");
                break;
        }
    }
}
