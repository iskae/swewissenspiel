package application.gui.observers;

import application.game.api.events.Player;
import application.gui.GUI;

public class PlayerListener implements Observer {

    public PlayerListener() {
    }

    @Override
    public final void update(final Object info) {
        if (info instanceof Player) {
            System.out.println("" + ((Player) info).toString() + " ist am Zug.");
            GUI.nextPlayer(true);
        }
    }
}
