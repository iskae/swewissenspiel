package application.gui.observers;

import application.game.api.events.Conflict;

public class ConflictListener implements Observer {

    public ConflictListener() {
    }

    @Override
    public final void update(final Object info) {
        if (info instanceof Conflict) {
            Conflict con = ((Conflict) info);
            if (con.getAggressor().getPlayer() == con.getVictim().getPlayer()
                    && con.getVictim() != null) {
                System.out.println("Eine ihrer Figuren steht schon auf diesem Feld.");
            }
            if (con.getAggressor().getPlayer() != con.getVictim().getPlayer()
                    && con.getVictim() != null) {
                System.out.println("Spieler " + con.getAggressor() + " greift Spieler " + con.getVictim() + " an.");
            }
        }
    }

}
