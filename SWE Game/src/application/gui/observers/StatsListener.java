package application.gui.observers;

import application.game.api.events.Stats;

public class StatsListener implements Observer {

    public StatsListener() {
    }

    @Override
    public final void update(final Object info) {
        if (info instanceof Stats) {
            System.out.println(((Stats) info).toString());
        }
    }

}
