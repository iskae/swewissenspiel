package application.gui.observers;

import application.game.api.events.FigureRequest;
import application.gui.GUI;

public class FigureRequestListener implements Observer {

    public FigureRequestListener() {
    }

    @Override
    public final void update(final Object info) {
        if (info instanceof FigureRequest) {
            GUI.figureRequest(true);
        }
    }
}
