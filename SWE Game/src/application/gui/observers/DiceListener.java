package application.gui.observers;

import application.game.api.events.Dice;

public class DiceListener implements Observer {

    public DiceListener() {
    }

    @Override
    public final void update(final Object info) {
        if (info instanceof Dice) {
            switch (((Dice) info).getPlayer()) {
                case 0:
                    System.out.println("Spieler Red hat eine " + ((Dice) info).getDice() + " gewürfelt.");
                    break;
                case 1:
                    System.out.println("Spieler Yellow hat eine " + ((Dice) info).getDice() + " gewürfelt.");
                    break;
                case 2:
                    System.out.println("Spieler Green hat eine " + ((Dice) info).getDice() + " gewürfelt.");
                    break;
                case 3:
                    System.out.println("Spieler Blue hat eine " + ((Dice) info).getDice() + " gewürfelt.");
                    break;
                default:
                    System.out.println("Spieler Unknown hat eine " + ((Dice) info).getDice() + " gewürfelt.");
                    break;
            }
        }
    }
}
