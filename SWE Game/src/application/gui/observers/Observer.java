package application.gui.observers;

public interface Observer<Info> {

    void update(Info info);

}
