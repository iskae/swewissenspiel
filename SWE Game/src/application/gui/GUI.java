package application.gui;

import application.game.stateMachine.StateMachineGUI;
import application.game.api.GameAPI;
import application.gui.observers.ConflictListener;
import application.gui.observers.CounterListener;
import application.gui.observers.DiceListener;
import application.gui.observers.FigureRequestListener;
import application.gui.observers.MoveListener;
import application.gui.observers.PlayerListener;
import application.gui.observers.StatsListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GUI {

    private static boolean reroll = false;
    private static boolean nextPlayer = false;
    private static boolean figureRequest = false;
    private static boolean categoryRequest = false;

    private StateMachineGUI gUIState;
    private BufferedReader reader;

    public GUI() {
        initGame();
        initGUI();
        System.out.println("Willkommen zum Wissenslaufspiel");
        startGUI();
    }

    private void initGame() {
        gUIState = new StateMachineGUI();
        while (!gUIState.getcurrentStateName().equals("GameStarted")) {
        }
        gUIState.transverse("WaitForDice");
    }

    private void initGUI() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    private void startGUI() {
        chooseStarter();
        String input = "";
        while (!gUIState.getcurrentStateName().equals("Exit")) {
            if (gUIState.getcurrentStateName().equals("WaitForDice")) {
                System.out.println("");
                System.out.println("Zum Würfeln drücken Sie „x“!");
                try {
                    input = reader.readLine();
                } catch (IOException ex) {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                switch (input) {
                    case "x":
                        pressDice();
                        gUIState.transverse("DiceProcess");
                        break;
                    case "stats":
                        showStats();
                        break;
                    case "exit":
                        System.out.println("Goodbye");
                        gUIState.transverse("Exit");
                        break;
                    default:
                        break;
                }
            }
            if (gUIState.getcurrentStateName().equals("WaitForFigure")) {
                System.out.println("");
                System.out.println("Wählen sie eine Figure [1-3]!");
                try {
                    input = reader.readLine();
                } catch (IOException ex) {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                switch (input) {
                    case "1":
                        pressFigure(0);
                        gUIState.transverse("FigureProcess");
                        break;
                    case "2":
                        pressFigure(1);
                        gUIState.transverse("FigureProcess");
                        break;
                    case "3":
                        pressFigure(2);
                        gUIState.transverse("FigureProcess");
                        break;
                    case "exit":
                        System.out.println("Goodbye");
                        gUIState.transverse("Exit");
                        break;
                    default:
                        break;
                }
            }
            if (gUIState.getcurrentStateName().equals("CategoryRequest")) {
            }
            if (reroll) {
                gUIState.transverse("WaitForDice");
                reroll = false;
            }
            if (nextPlayer) {
                gUIState.transverse("WaitForDice");
                nextPlayer = false;
            }
            if (figureRequest) {
                gUIState.transverse("WaitForFigure");
                figureRequest = false;
            }
            if (categoryRequest) {
                gUIState.transverse("CategoryRequest");
                categoryRequest = false;
            }
        }
    }

    private void chooseStarter() {
        PlayerListener playi = new PlayerListener();
        GameAPI.gameapi.getGameSub().attach(playi);
        GameAPI.gameapi.chooseStarter();
        GameAPI.gameapi.getGameSub().detach(playi);
    }

    private void pressDice() {
        ConflictListener confy = new ConflictListener();
        CounterListener county = new CounterListener();
        PlayerListener playi = new PlayerListener();
        DiceListener dicey = new DiceListener();
        MoveListener movy = new MoveListener();
        StatsListener staty = new StatsListener();
        FigureRequestListener figy = new FigureRequestListener();
        GameAPI.gameapi.getGameSub().attach(confy);
        GameAPI.gameapi.getGameSub().attach(county);
        GameAPI.gameapi.getGameSub().attach(playi);
        GameAPI.gameapi.getGameSub().attach(dicey);
        GameAPI.gameapi.getGameSub().attach(movy);
        GameAPI.gameapi.getGameSub().attach(staty);
        GameAPI.gameapi.getGameSub().attach(figy);
        GameAPI.gameapi.dice();
        GameAPI.gameapi.getGameSub().detach(confy);
        GameAPI.gameapi.getGameSub().detach(county);
        GameAPI.gameapi.getGameSub().detach(playi);
        GameAPI.gameapi.getGameSub().detach(dicey);
        GameAPI.gameapi.getGameSub().detach(movy);
        GameAPI.gameapi.getGameSub().detach(staty);
        GameAPI.gameapi.getGameSub().detach(figy);
    }

    private void pressFigure(final int index) {
        ConflictListener confy = new ConflictListener();
        PlayerListener playi = new PlayerListener();
        MoveListener movy = new MoveListener();
        StatsListener staty = new StatsListener();
        FigureRequestListener pigy = new FigureRequestListener();
        GameAPI.gameapi.getGameSub().attach(confy);
        GameAPI.gameapi.getGameSub().attach(playi);
        GameAPI.gameapi.getGameSub().attach(movy);
        GameAPI.gameapi.getGameSub().attach(staty);
        GameAPI.gameapi.getGameSub().attach(pigy);
        GameAPI.gameapi.moveFigure(index);
        GameAPI.gameapi.getGameSub().detach(confy);
        GameAPI.gameapi.getGameSub().detach(playi);
        GameAPI.gameapi.getGameSub().detach(movy);
        GameAPI.gameapi.getGameSub().detach(staty);
        GameAPI.gameapi.getGameSub().detach(pigy);
    }

    private void showStats() {
        StatsListener staty = new StatsListener();
        GameAPI.gameapi.getGameSub().attach(staty);
        GameAPI.gameapi.showStats();
        GameAPI.gameapi.getGameSub().detach(staty);
    }

    public static void reroll(final boolean set) {
        reroll = set;
    }

    public static void nextPlayer(final boolean set) {
        nextPlayer = set;
    }

    public static void figureRequest(final boolean set) {
        figureRequest = set;
    }

    public static void categoryRequest(final boolean set) {
        categoryRequest = set;
    }
}
