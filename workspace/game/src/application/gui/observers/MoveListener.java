package application.gui.observers;

import application.game.api.events.Move;

public class MoveListener implements Observer {

    public MoveListener() {
    }

    @Override
    public final void update(final Object info) {
        if (info instanceof Move) {
            Move mov = ((Move) info);
            mov.getTo().setOccupier(mov.getFrom().getOccupier());
        }
    }

}
