package application.gui.observers;

import application.game.api.events.Counter;
import application.gui.GUI;


public class CounterListener implements Observer {

    public CounterListener() {
    }

    @Override
    public final void update(final Object info) {
        if (info instanceof Counter) {
            switch (((Counter) info).getCount()) {
            case 2:
                System.out.println("Zweiter Versuch!");
                GUI.reroll(true);
                break;
            case 1:
                System.out.println("Letzer Versuch!");
                GUI.reroll(true);
                break;
            default:
                // Abgelaufen oder neustart in beiden Falle keine Ausgabe
                break;
            }
        }
    }
    
}
