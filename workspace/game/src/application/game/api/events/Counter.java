package application.game.api.events;

/**
 *
 * @author Administrator
 */
public class Counter implements Info {
    private final int count;

    public Counter(final int count) {
        this.count = count;
    }

    public final int getCount() {
        return this.count;
    }
}
