package application.game.api.events;

public class Dice implements Info {

    private final int dice;
    private final int player;

    public Dice(int dice, int player) {
        this.dice = dice;
        this.player = player;
    }

    public final int getPlayer() {
        return this.player;
    }

    public final int getDice() {
        return this.dice;
    }
}
