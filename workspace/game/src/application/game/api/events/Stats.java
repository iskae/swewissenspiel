package application.game.api.events;

import application.game.api.events.Info;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class Stats implements Info {

    private final Player player;
    private final ArrayList<Figure> figures;
    private final ArrayList<Integer> fieldnumber;

    public Stats(final Player player, final ArrayList<Figure> figures, final ArrayList<Integer> fieldnumber) {
        this.player = player;
        this.figures = figures;
        this.fieldnumber = fieldnumber;
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder("");
        sb.append(player.toString()).append(": ");
        sb.append(figures.size());
        sb.append(" von 3 Figuren im Spiel");
        if (!figures.isEmpty()) {
            for (int i = 0; i < figures.size(); i++) {
                if (i == 0) {
                    sb.append(": ");
                } else {
                    sb.append(", ");
                }
                sb.append(figures.get(i).toString());
                sb.append(" Feld ");
                sb.append(fieldnumber.get(i).toString());
            }
        }
        return sb.toString();
    }
}
