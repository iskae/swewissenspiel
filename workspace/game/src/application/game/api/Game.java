package application.game.api;

public interface Game {

    void chooseStarter();

    void dice();

    void moveFigure(int index);

    void showStats();

}
