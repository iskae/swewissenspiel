package application.game.api;

public interface GameAPI {

    static GameAPI gameapi = GameAPIImpl.makeGameAPI();

    Game getGame();

    Subject getGameSub();

    void chooseStarter();

    void dice();

    void moveFigure(int index);

    void showStats();
}
