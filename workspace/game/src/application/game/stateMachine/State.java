package application.game.stateMachine;

public class State {

    private String stateName = "";

    public State(final String name) {
        this.stateName = name;
    }

    public final String getStateName() {
        return this.stateName;
    }
}
